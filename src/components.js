import Vue from "vue";
import Capture from "@/components/Capture.vue";
import DatePicker from "@/components/DatePicker.vue";
import Preloader from "@/components/Preloader.vue";
import SelectDialog from "@/components/SelectDialog.vue";
import StatusBar from "@/components/StatusBar.vue";
import TheHeader from "@/components/TheHeader.vue";

Vue.component("preloader", Preloader);
Vue.component("date-pciker", DatePicker);
Vue.component("status-bar", StatusBar);
Vue.component("the-header", TheHeader);
Vue.component("v-select-dialog", SelectDialog);
Vue.component("v-capture", Capture);

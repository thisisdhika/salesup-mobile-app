import lodash from "lodash";
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./plugins";
import "./registerServiceWorker";
import "./theme";
import "./validator";
import "./components"; // Register global components

window._ = lodash;

Vue.config.productionTip = false;
Vue.prototype.$snackbar = function(type, text) {
  this.$root.snackbar = true;
  this.$root.snackbarText = text;
  this.$root.snackbarType = type;
};

const app = new Vue({
  router,
  store,
  data: {
    snackbar: false,
    snackbarText: null,
    snackbarType: null
  },
  render: h => h(App)
}).$mount("#app");

window.app = app;

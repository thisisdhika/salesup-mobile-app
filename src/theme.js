import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";
import "@/sass/main.scss";

Vue.use(Vuetify, {
  theme: {
    primary: "#4C2A86",
    secondary: "#07B3BA"
  },
  minifyTheme: function(val) {
    return process.env.NODE_ENV === "production"
      ? val.replace(/[\s|\r\n|\r|\n]/g, "")
      : null;
  }
});

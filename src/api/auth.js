import Vue from "vue";
import axios from "axios";
import { API_ENDPOINT, API_CLIENT_ID, API_CLIENT_SECRET } from "../settings";

export default {
  login(creds) {
    return new Promise((resolve, reject) => {
      const data = {
        grant_type: "password",
        client_id: API_CLIENT_ID,
        client_secret: API_CLIENT_SECRET,
        ...creds,
        scope: ""
      };
      axios
        .post(`${API_ENDPOINT}/oauth/token`, data)
        .then(({ data }) => {
          const tokens = data;
          localStorage.setItem("access_token", data.access_token);

          axios
            .get(`${API_ENDPOINT}/api/user`, {
              headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                Authorization: "Bearer " + data.access_token
              }
            })
            .then(({ data }) => {
              const user = { user: data.data };
              const response = Object.assign({}, user, tokens);

              resolve(response);
            })
            .catch(error => {
              if (error.response) error = error.response;

              reject(error);
            });
        })
        .catch(error => {
          if (error.response) {
            if (error.response.data.error === "invalid_client") {
              error.response.data.message = "NIK or Password is invalid.";
            }
            error = error.response;
          } else {
            error.data = {};
            error.data.error = "unknown_error";
            error.data.message = "Unknown Error";
          }
          reject(error);
        });
    });
  },
  logout() {
    return new Promise((resolve, reject) => {
      localStorage.removeItem("access_token");

      resolve();
    });
  },
  check() {
    const token = localStorage.getItem("access_token");
    if (token) {
      return true;
    }
    return false;
  },
  getAuthHeader() {
    return {
      Authorization: "Bearer " + localStorage.getItem("access_token")
    };
  },
  me() {
    const me = localStorage.getItem("me");

    return me ? JSON.parse(me) : null;
  }
};

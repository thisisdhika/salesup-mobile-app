import { Plugins } from "@capacitor/core";
import swal from "sweetalert";

const { Geolocation, Toast } = Plugins;

export default {
  getCurrentPosition() {
    return new Promise((resolve, reject) => {
      const options = {
        enableHighAccuracy: true
      };

      const getPosition = position => {
        resolve(position.coords);
      };

      const errorPosition = error => {
        swal({
          text:
            "Position could not be determined, please make sure your gps is active."
        }).then(() => {
          router.push("/");
        });

        reject({
          latitude: null,
          longitude: null
        });
      };

      navigator.geolocation.getCurrentPosition(
        getPosition,
        errorPosition,
        options
      );
      navigator.geolocation.watchPosition(getPosition, errorPosition, options);
    });
  }
};

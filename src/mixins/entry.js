export default {
  data: () => ({
    form: {}
  }),
  methods: {
    validate() {
      return new Promise((resolve, reject) => {
        this.$validator.validateAll().then(valid => {
          if (valid) {
            resolve(this.form);
            return;
          }

          const text = "There are some errors in your submission. Please correct them.";

          this.scrollToTop();

          this.$swal({
            title: "Oops!",
            text,
            type: "error"
          });

          reject(false);
        });
      });
    },
    scrollToTop() {
      const app = document.querySelector(".application");

      app.scrollTop = 0;
    }
  }
};

import Vue from "vue";
import Swal from "sweetalert2";

Vue.prototype.$swal = function(options) {
  return Swal.fire(options);
};

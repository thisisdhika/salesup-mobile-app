import Vue from "vue";
import axios from "axios";
import { API_ENDPOINT, API_PATH } from "../settings";

const $request = axios.create({
  baseURL: `${API_ENDPOINT}${API_PATH}`
});

Vue.prototype.$request = $request;

$request.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

import auth from "../interceptor";

// Add a request interceptor
$request.interceptors.request.use(auth.request, auth.requestError);

// Add a response interceptor
$request.interceptors.response.use(auth.response, auth.responseError);

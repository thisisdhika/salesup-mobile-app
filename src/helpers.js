export const range = (start, end) => {
  return Array.from({ length: end - start }, (v, k) => k + start);
};

export const pad = (num, size) => {
  let s = num + "";
  while (s.length < size) {
    s = "0" + s;
  }
  return s;
};

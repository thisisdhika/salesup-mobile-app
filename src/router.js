import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import auth from "./api/auth";

Vue.use(Router);

const router = new Router({
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/login",
      component: () => import("./views/Login.vue"),
      meta: { guest: true }
    },
    {
      path: "/",
      name: "home",
      component: Home,
      meta: { auth: true }
    },
    {
      path: "/entry/add",
      name: "new-entry",
      component: () => import("./views/Entry/Entry.vue"),
      meta: { auth: true }
    },
    {
      path: "/entry/:id",
      name: "detail-entry",
      component: () => import("./views/Detail/Detail.vue"),
      meta: { auth: true }
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.auth) && !auth.check()) {
    next({ path: "/login" });
    // next({ path: '/user/login', query: { redirect: to.fullPath } });
  } else if (to.matched.some(record => record.meta.guest) && auth.check()) {
    next({ path: "/" });
    // next({ path: '/', query: { redirect: to.fullPath } });
  } else {
    next();
  }
});

window.router = router;
export default router;

export default {
  SET_BOOKING_DATA(state, data) {
    state.newBooking = Object.assign({}, state.newBooking, data);
  },
  SET_BUILDINGS(state, data) {
    state.buildings = data;
  },
  SET_FLOORS(state, data) {
    state.floors = data;
  },
  SET_DESKS(state, data) {
    state.desks = data;
  }
};

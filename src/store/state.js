export default {
  buildings: [],
  deviceInfo: {
    build_number: null,
    brand: null,
    device_name: null,
    model: null,
    os: null,
    os_version: null
  },
  desks: [],
  floors: [],
  myBooks: {
    today: null,
    tomorrow: null
  },
  newBooking: {
    building: null,
    date: null,
    desk: null,
    floor: null,
    time: null
  }
};
